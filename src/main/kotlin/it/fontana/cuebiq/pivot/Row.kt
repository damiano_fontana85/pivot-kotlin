package it.fontana.cuebiq.pivot

data class Row(val classifiers: Map<String,String>,val value: Number) {
	
	public fun getClassifiersValue(classifier: String ): String? {
		return classifiers.get(classifier);
	}
}
package it.fontana.cuebiq.pivot.tree.visitor
import  it.fontana.cuebiq.pivot.Evaluation
import it.fontana.cuebiq.pivot.tree.Leaf;
import it.fontana.cuebiq.pivot.tree.Node;
import it.fontana.cuebiq.pivot.util.Assert;


data class EvaluationVisitor(val func : (List<Evaluation>) -> Evaluation) : Visitor<Unit> {
	
	
	override fun visit(node: Node): Evaluation {
		
		val evaluations = node.childs.map{ it -> it.evaluation }.filterNotNull()
		return  func(evaluations);		
			
	}

	
	override fun visit(leaf: Leaf ): Evaluation {
		
		val evaluations = leaf.values.map{ it -> Evaluation(1L, it )}
		return func(evaluations);
					
	}


	override fun  isTopDown(): Boolean = false
	

	override fun getResult() = Unit

	
}
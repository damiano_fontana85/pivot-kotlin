package it.fontana.cuebiq.pivot.tree

import it.fontana.cuebiq.pivot.tree.visitor.Visitor
import it.fontana.cuebiq.pivot.util.Assert;


class Leaf : Tree {

	
	constructor(key : String, level: Int) : super(key,level)
	
	var values = ArrayList<Int>();
 
	
	public override fun accept(v: Visitor<*>) {
		Assert.notNull(v, "visitor must be not null");
	
		super.evaluation = v.visit(this);
		
	}
	
	
	
	public override fun insert(nodes: List<String> ,value: Int) {
		values.add(value)
			
	}

	public override fun search(nodes: List<String> ): Tree? {
		if(!nodes.isEmpty())
			return null
		else
			return this

	}

	
	public override fun getChild(key: String): Tree? {
		return null
	}
	
	
	
	public override fun toString(): String {
		val values = values.map { it.toString() }.joinToString(" ")
		val evaluation = super.evaluation?.value ?: ""
		return super.toString() + "-> $values = $evaluation"
		
	}
}
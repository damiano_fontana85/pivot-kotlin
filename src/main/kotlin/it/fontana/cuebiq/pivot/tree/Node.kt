package it.fontana.cuebiq.pivot.tree

import java.util.*
import it.fontana.cuebiq.pivot.tree.visitor.Visitor
import it.fontana.cuebiq.pivot.util.Assert;


class Node : Tree {
	
	
	constructor(key : String, level: Int) : super(key,level)
	
	var childs =  ArrayList<Tree>()
	
	var locate = HashMap<String, Tree>()
	
	

	override fun accept(v :Visitor<*>) {
		Assert.notNull(v, "visitor can't be null");
		
		if(v.isTopDown())
		{
			super.evaluation = v.visit(this);
			childs.stream().forEachOrdered{t -> t.accept(v)}
		}
		else {
			childs.stream().forEachOrdered{t -> t.accept(v)}
			super.evaluation = v.visit(this);
		}
	}

	
	override fun insert(nodes: List<String> ,value: Int) {
		if(nodes.isEmpty())
			return
			
		val nodeName = nodes.get(0)
		var node: Tree? 

		if(locate.containsKey(nodeName))
			node = locate.get(nodeName)
		else {
			node = if(nodes.size == 1) Leaf(nodeName,level+1) else  Node(nodeName,level+1);
			childs.add(node);
			locate.put(nodeName, node);
		}

		node?.insert(nodes.subList(1, nodes.size),value);
			
	}

	override fun search(nodes: List<String> ): Tree? {
		if(nodes.isEmpty())
			return this

		val nodeName = nodes.get(0);
		
		val node = locate.get(nodeName);
		return node?.search(nodes.subList(1, nodes.size))
	}

	
	override fun getChild(key: String): Tree? {
		return locate.get(key)

	}
	
	
	public override fun toString(): String {
		val value = super.evaluation?.value ?: ""
		return super.toString() +" = $value"
	}

}
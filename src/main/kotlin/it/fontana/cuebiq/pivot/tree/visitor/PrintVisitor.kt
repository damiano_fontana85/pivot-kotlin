package it.fontana.cuebiq.pivot.tree.visitor
import  it.fontana.cuebiq.pivot.Evaluation
import it.fontana.cuebiq.pivot.tree.Leaf;
import it.fontana.cuebiq.pivot.tree.Node;
import it.fontana.cuebiq.pivot.util.Assert;


class PrintVisitor() : Visitor<String> {
	
	
	final val indent = 2;

	var line = "";
	
	override fun visit(node: Node): Evaluation {
		val inc = 1.rangeTo( indent*node.level).map { " " }.joinToString(" ")
		line = "$line $inc$node \n";
		
		return node.evaluation ?: Evaluation(-1L,-1);
			
	}

	
	override fun visit(leaf: Leaf ): Evaluation {
		
		val inc = 1.rangeTo( indent*leaf.level).map { " " }.joinToString(" ")
		
		line = "$line $inc$leaf \n";
		
		return leaf.evaluation ?: Evaluation(-1L,-1);
					
	}


	override fun  isTopDown(): Boolean = true
	

	override fun getResult() = line

	
}
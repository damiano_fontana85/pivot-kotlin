package it.fontana.cuebiq.pivot.tree

import it.fontana.cuebiq.pivot.Evaluation
import it.fontana.cuebiq.pivot.tree.visitor.Visitor

abstract class Tree(val key : String, val level: Int) {
	
	var evaluation: Evaluation? = null
		
	abstract fun accept(v: Visitor<*>);
	
	abstract fun insert(nodes: List<String> ,value: Int)

	abstract fun search(nodes: List<String> ): Tree? 

	
	abstract fun getChild(key: String): Tree? 


	override public fun toString(): String = key



}
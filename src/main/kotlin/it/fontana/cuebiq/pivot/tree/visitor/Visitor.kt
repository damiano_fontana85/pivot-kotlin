package it.fontana.cuebiq.pivot.tree.visitor
import it.fontana.cuebiq.pivot.Evaluation
import it.fontana.cuebiq.pivot.tree.*

interface Visitor<T> {
	fun visit(node: Node ): Evaluation
	fun visit(leaf: Leaf ): Evaluation
	fun isTopDown(): Boolean
	fun getResult(): T;
}
package it.fontana.cuebiq.pivot

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import it.fontana.cuebiq.pivot.tree.Node;
import it.fontana.cuebiq.pivot.tree.Tree;
import it.fontana.cuebiq.pivot.tree.visitor.EvaluationVisitor;
import it.fontana.cuebiq.pivot.util.Assert;

object Pivot {
	
	/**
	 * @param data i dati classificati 
	 * @param fun la funzione usata per calcolare 
	 * @param aggregations la lista di aggregazioni
	 * @return l'albero di aggregazione 
	 */
	public fun  evaluateData(data: List<Row> ,func: (List<Evaluation>) -> Evaluation ,vararg aggregations: String): Tree {
		Assert.notNull(data, "data must be defined");
		Assert.isTrue(aggregations.size != 0, "at least one aggregation must be defined");
		Assert.notNull(func, "aggregation function must be defined");
		
		val tree = buildTree(data, *aggregations);
		tree.accept(EvaluationVisitor(func));
		return tree;
	}
	
	
	/**
	 * Questo metodo crea l'albero di aggregazione a partire dalle colonne di aggregazione
	 * definite
	 * 
	 * @param data i dati classificati
	 * @param aggregations la lista di aggregazioni
	 * @return l'albero di aggregazione senza valori calcolati
	 */
	
	fun buildTree(data: List<Row> ,vararg aggregation: String): Tree {

		val key = "root";
		val tree =  Node(key,0);
		data.map{ it -> getPathFromAggregation(it, *aggregation)}.forEach{ it -> tree.insert(it.classifiers, it.value)}

		return tree;
	}



	fun  getPathFromAggregation(row: Row , vararg aggregation: String): ClassifiersAndValue{
		val rowContainsAllAggreagation = aggregation.all{ it ->  row.classifiers.containsKey(it) }
		
		
		Assert.isTrue(rowContainsAllAggreagation, "row must contains all aggregations");
		
		val classifiers: List<String> = aggregation.map{ it ->  row.classifiers.get(it) }.filterNotNull()
		return ClassifiersAndValue(classifiers, row.value.toInt());
		
	}



	public  data class ClassifiersAndValue(val classifiers : List<String>, val value: Int)
	{
		
	}


}